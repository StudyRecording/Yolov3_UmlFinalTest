# Yolov3_UmlFinalTest

#### 介紹
UML期末考查项目  

# 项目成员  
胡鹏成：下载编译源程序，训练物体检测模型，完成用例图  
徐晓庆：下载编译源程序，搜集数据，完成时序图   
罗涌甲：下载编译源程序，数据预处理，完成用例图   
任洪正：下载编译源程序，完成活动图   
刘如意：下载编译源程序，完成用例图   
田士勇：下载编译源程序，完成活动图   
张帝  ：下载编译源程序，完成时序图   
卢永强：下载编译源程序，完成组件图   

# 项目架构
|— UML图     
|  
|— 源程序  
|  
|— 物体检测模型  
|  
|— 课程设计报告     
|  
|— README.md  


# 各个文件功能
| 文件名 | 功能 |  
| ----- | ----- |
|UML图|存放项目成员画的UML图|
|源程序|存放项目源代码|
|物体检测模型|https://pan.baidu.com/s/1jKuO6SGcIB2rrVfxrbWetw |
|课程设计报告|存放项目成员的课程设计报告|
| README.md | 项目的相关说明，即本文档 |

**注意：**   

1. 项目原始项目地址为：https://github.com/AlexeyAB/darknet

2. 本项目为UML课程的期末考察  

# DarkNet在windows上编译运行
1. 配置OpenCV（版本为3.3.0及其以下）配置教程：https://blog.csdn.net/luojie140/article/details/78387279  
2. 下载源代码进行解压，用Visual Studio 2017打开  （安装CUDA）  \darknet-master\darknet-master\build\darknet\darknet.sln文件，如果未安装CUDA，则打开\darknet-master\darknet-master\build\darknet\darknet_no_gpu.sln文件  
3. 编译生成程序。

# 程序在Windows上的使用
在\darknet-master\darknet-master\build\darknet目录下：  
1. darknet_yolo_v3.cmd- 初始化为236 MB Yolo v3 COCO模型 yolov3.weights＆yolov3.cfg并在图像上检测dog.jpg并显示。  
2. darknet_voc.cmd - 使用194 MB VOC模型yolo-voc.weights和yolo-voc.cfg进行初始化并等待输入图像文件的名称
3. arknet_demo_voc.cmd - 使用194 MB VOC模型yolo-voc.weights和yolo-voc.cfg进行初始化并播放您必须重命名为的视频文件：test.mp4  
4. darknet_demo_store.cmd - 使用194 MB VOC模型yolo-voc.weights和yolo-voc.cfg进行初始化并播放您必须重命名为：test.mp4的视频文件，并将结果存储到：res.avi  
5. darknet_net_cam_voc.cmd - 使用194 MB VOC模型进行初始化，播放来自网络摄像机mjpeg-stream的视频（也来自您的手机）  
6. darknet_coco_9000.cmd - 使用186 MB Yolo9000 COCO模型进行初始化，并在图像上显示检测：dog.jpg  
7. darknet_coco_9000_demo.cmd - 使用186 MB Yolo9000 COCO模型进行初始化，并在视频上显示检测结果（如果存在）：street4k.mp4，并将结果存储到：res.avi  

### 在命令行上使用（Windows中的powershell窗口或者cmd窗口）  
1. Yolo v3 COCO - 图片：darknet.exe detector test data/coco.data cfg/yolov3.cfg yolov3.weights -i 0 -thresh 0.25  
2. 对象的输出坐标： darknet.exe detector test data/coco.data yolov3.cfg yolov3.weights -ext_output dog.jpg  
3. Yolo v3 COCO - 视频：darknet.exe detector demo data/coco.data cfg/yolov3.cfg yolov3.weights -ext_output test.mp4  
4. Yolo v3 COCO - WebCam 0：darknet.exe detector demo data/coco.data cfg/yolov3.cfg yolov3.weights -c 0  
5. 网络视频摄像的Yolo v3 COCO - Smart WebCam：darknet.exe detector demo data/coco.data cfg/yolov3.cfg yolov3.weights http://192.168.0.80:8080/video?dummy=param.mjpg  
6. Yolo v3 - 将结果保存到文件res.avi：darknet.exe detector demo data/coco.data cfg/yolov3.cfg yolov3.weights -thresh 0.25 test.mp4 -out_filename res.avi  
7. Yolo v3 Tiny COCO - 视频：darknet.exe detector demo data/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights test.mp4  
8. Yolo v3 Tiny on GPU＃0：darknet.exe detector demo data/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights -i 0 test.mp4  
9. 替代方法Yolo v3 COCO - 图像： darknet.exe detect cfg/yolov3.cfg yolov3.weights -i 0 -thresh 0.25  
10. 186 MB Yolo9000 - 图片： darknet.exe detector test cfg/combine9k.data yolo9000.cfg yolo9000.weights  
11. 如果您使用cpp api构建应用程序，请记住将data / 9k.tree和data / coco9k.map放在应用程序的同一文件夹下处理图像列表data/train.txt并保存检测结果以result.txt供使用：darknet.exe detector test cfg/coco.data yolov3.cfg yolov3.weights -dont_show -ext_output < data/train.txt > result.txt  

# 培训数据  
我自己训练的物体检测模型（狗的检测模型）：https://pan.baidu.com/s/1jKuO6SGcIB2rrVfxrbWetw  

## 培训过程
1. 下载卷积层的预先训练的权重（154 MB）：http：//pjreddie.com/media/files/darknet53.conv.74 并放到目录中build\darknet\x64  
2. yolov3.cfg复制站体重命名为yolo-obj.cfg  
    - 修改 batch=64
    - 修改 subdivisions=8
    - 将每个[yolo]层中的classes值改为要检测类的数量  
    - 更改每个[yolo]层前的[convolutional]中的filters值，值为 filters =（classes + 5）x3  
3. 在目录build\darknet\x64\data\中创建文件obj.names  
在文件中写入要检测物体的名字，每一种物体的名字占单独的一行  
4. 在目录build\darknet\x64\data\中创建文件obj.data，并作出如下修改  
    ```
    classes= 检测物体的对象数
    train  = data/train.txt
    valid  = data/test.txt
    names = data/obj.names
    backup = backup/
    ```
5. 您应该在数据集的图像上标记每个对象。使用这个可视GUI软件标记有界的对象框并为Yolo v2和v3生成注释文件：https：//github.com/AlexeyAB/Yolo_mark  

6. 将处理号的图片和.txt文件放到build\darknet\x64\data\obj\文件夹下，将train.txt文件放入build\darknet\x64\data\中。

7. 开始训练：darknet.exe detector train data/obj.data yolo-obj.cfg darknet53.conv.74  

8. 每1000次迭代将生成一个yolo-obj_xxx.weights放在build\darknet\x64\backup\文件中，训练结束后 -yolo-obj_final.weights从路径build\darknet\x64\backup\获得结果。

9. 验证精确度
    - 利用序号5的方法制作测试集  
    - 修改.cfg文件如下
        ```
        [net]
        # Testing
        batch=1
        subdivisions=1
        # Training
        #batch=64
        #subdivisions=8
        ```
    - 修改test.txt文件，里面写入测试集图片的路径（利用序号5的方法），并将test.txt文件放入\darknet-master\darknet-master\build\darknet\x64\data\  
    - 验证精度，输入指令：darknet.exe detector test data/obj.data yolo-obj.cfg yolo-obj_xxx.weights


**注意：** 
1. 在每100次迭代后，会生成或更新yolo-obj_last.weights文件。当每个100次迭代后，可关闭程序，事后再用yolo-obj_last.weights文件继续进行训练，继续训练的命令为：darknet.exe detector test data/obj.data yolo-obj.cfg yolo-obj_last.weights。   

2. 在用GPU训练中发生Out of memory错误，可在.cfg文件中增加subdivisions的值为16，32或64。  

3. 训练中或者训练完成后，会在\darknet-master\darknet-master\build\darknet\x64\backup目录中生成一个或多个.wights文件。应选取表现最为优秀的文件。

